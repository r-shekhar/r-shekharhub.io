<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags always come first -->
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>If Taxi Trips were Fireflies: 1.3 Billion NYC Taxi Trips Plotted | Ravi Shekhar's Technical Blog
</title>
  <link rel="canonical" href="../posts/incandescent-taxi-view.html">



  <link rel="stylesheet" href="../theme/css/bootstrap.min.css">
  <link rel="stylesheet" href="../theme/css/font-awesome.min.css">
  <link rel="stylesheet" href="../theme/css/pygments/default.min.css">
  <link rel="stylesheet" href="../theme/css/style.css">


<meta name="description" content="I produce glowing visualizations of all 1.3 billion NYC Taxi trips, using Dask and Datashader. I also make a cleaned version of the Taxi Dataset available in Parquet format.">
<script>
  (function(i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function() {
      (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o);
    a.async = 1;
    a.src = g;
    m = s.getElementsByTagName(o)[0];
    m.parentNode.insertBefore(a, m)
  })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
  ga('create', 'UA-8241620-2', 'auto');
  ga('send', 'pageview');
</script>
</head>

<body>
  <header class="header">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <a href="../">
            <img class="img-fluid" src=../images/Self_small.jpg width=150 alt="Ravi Shekhar's Technical Blog">
          </a>
        </div>
        <div class="col-sm-8">
          <h1 class="title"><a href="../">Ravi Shekhar's Technical Blog</a></h1>
          <p class="text-muted">A Technical Blog of the Data Science Process</p>
          <ul class="list-inline">
            <li class="list-inline-item"><a href="https://github.com/r-shekhar" target="_blank">Github</a></li>
            <li class="list-inline-item"><a href="https://www.linkedin.com/in/rshekhar2/" target="_blank">LinkedIn</a></li>
            <li class="list-inline-item text-muted">|</li>
            <li class="list-inline-item"><a href="../pages/about.html">About</a></li>
          </ul>
        </div>
      </div>
    </div>
  </header>

  <div class="main">
    <div class="container">
      <h1>If Taxi Trips were Fireflies: 1.3 Billion NYC Taxi Trips Plotted
</h1>
      <hr>
<article class="article">
  <header>
    <ul class="list-inline">
      <li class="list-inline-item text-muted" title="2017-06-08T00:00:00-04:00">
        <i class="fa fa-clock-o"></i>
        2017 Jun 08
      </li>
      <li class="list-inline-item">
        <i class="fa fa-folder-open-o"></i>
        <a href="../category/data-analysis.html">Data Analysis</a>
      </li>
      <li class="list-inline-item">
        <i class="fa fa-files-o"></i>
        <a href="../tag/python.html">#python</a>,         <a href="../tag/taxi.html">#Taxi</a>,         <a href="../tag/dask.html">#Dask</a>      </li>
    </ul>
  </header>
  <div class="content">
    <p>The NYC Taxi and Limousine Commission has publicly released a dataset of taxi trips from January 2009 - June 2016 with GPS coordinates for starting and endpoints. This dataset has been analyzed fairly extensively, most notably by <a href="http://toddwschneider.com/posts/analyzing-1-1-billion-nyc-taxi-and-uber-trips-with-a-vengeance/">Todd W. Schneider</a>, who produced some really nice summaries of the dataset. <a href="http://chriswhong.com">Chris Whong</a> originally got the dataset released publicly in the first place with a FOIA request, and has produced a famous visualization, <a href="http://chriswhong.github.io/nyctaxi/">NYC Taxis: A Day in the Life</a>. <a href="http://tech.marksblogg.com/benchmarks.html">Mark Litwintschick</a> benchmarked various relational database and big data technologies using this dataset. </p>
<p>I downloaded the data files from <a href="http://www.nyc.gov/html/tlc/html/about/trip_record_data.shtml">TLC website</a>, and (<a href="https://github.com/r-shekhar/NYC-transport">very</a> <a href="https://medium.com/towards-data-science/geospatial-operations-at-scale-with-dask-and-geopandas-4d92d00eb7e8">painfully</a>) using Python, Dask, and Spark, have produced a cleaned dataset in Parquet format, which I make this available for AWS users at the end of this post. </p>
<p>So I was curious, where do taxis pick up passengers, or more precisely, what does the distribution of taxi pickup locations look like? With 1.3 billion taxi pickups, plotting the distribution in a way that does not wash out detail is very challenging. Scatter plots are useless due to overplotting, and 2D histograms are a form of kernel density estimation that necessarily blur or pixelate a lot of the details. Additionally, with the full dataset, the pickup locations alone  total 21GB, which is more than the memory of my 16GB laptop. Out of core tools can solve that technical problem easily (and subsampling is easier than that), but what about the visual problem? Human eyes are incapable of absorbing 21GB of information in a plot.</p>
<p>The solution to this comes from an interesting library called <a href="https://github.com/bokeh/datashader">Datashader</a>. It dynamically generates generate a 2D Histogram at the resolution of your display (or a specified canvas). Each pixel on the display corresponds to certain histogram boundaries in the data. The library counts the number of data points that fall within those boundaries for each pixel, and this number is used to color the intensity of the pixel. Leveraging Dask, the creation of the histogram can scale to terabytes of data, and be spread across a cluster. Leveraging Bokeh, the final plot can be zoomed and panned. Using techniques from <a href="https://en.wikipedia.org/wiki/High-dynamic-range_imaging">high dynamic range</a> photography, intensity ranges are mapped so that maximum contrast is present at any zoom level, and in any given view. This is what the map of taxi pickup locations (1.3 billion points) looks like over Manhattan, plotted using the viridis perceptually uniform colormap.</p>
<p><img alt="NYC Taxi pickups map for Manhattan" src="../images/pickups_narrow.jpg" width="100%" >
<em><a href="../images/pickups_narrow.jpg">Click for full resolution</a></em></p>
<p>The first thing I notice is how clearly I can see the street patterns. In parts of Brooklyn and Queens, the street pattern is sharp. In Manhattan, the pattern is `fuzzier', especially near the southern tip of Manhattan and in Midtown south of Central Park. There are an awful lot of pickups that, according to GPS coordinates, fall over the Hudson or East rivers, and quite a few pickups that fall in the portion of Central park where there are no roads. Obviously, not a lot of taxi trips are starting in the rivers surrounding Manhattan, but what this plot shows is instead how important GPS error is. The fuzziness arises from tall buildings which make it quite difficult to get a good GPS fix, and the taller the buildings, the fuzzier the streets look. More broadly, the Midtown area south of Central Park is very bright, indicating a lot of taxi trips start there. </p>
<p><img alt="Taxi pickups map for NYC Metro Area" src="../images/pickups.jpg" width="100%" ><em><a href="../images/pickups.jpg">Click for full resolution</a></em></p>
<p>The second image is also taxi pickups, but on a much wider scale. Zoomed out, most of Manhattan lights up like a beacon. The other boroughs are a lot darker, but the airports, JFK and La Guardia in particular, also light up. Now let's examine the dropoff locations.</p>
<p><img alt="NYC Taxi dropoffs map for Manhattan" src="../images/dropoffs_narrow.jpg" width="100%" >
<em><a href="../images/dropoffs_narrow.jpg">Click for full resolution</a></em></p>
<p><img alt="Taxi dropoffs map for NYC Metro Area" src="../images/dropoffs.jpg" width="100%" >
<em><a href="../images/dropoffs.jpg">Click for full resolution</a></em></p>
<p><img alt="Taxi dropoffs map for NYC Metro Area" src="../images/dropoffs_tz.png" width="100%" >
<em><a href="../images/dropoffs_tz.png">Click for full resolution</a></em></p>
<!-- In a [previous](https://medium.com/towards-data-science/geospatial-operations-at-scale-with-dask-and-geopandas-4d92d00eb7e8) [post](https://r-shekhar.github.io/posts/spatial-joins-geopandas-dask.html), I produced a heatmap of taxi dropoffs, which I show again here.

{% img ../images/dropoffs_tz.jpg 800 %}

While it's straightforward to see Manhattan and the airports have a lot of dropoffs, a lot of information is being lost in the coarse binning of dropoff locations into taxi zones. What the map does not show is that approximately 3% of taxi trips 

The question is, can we do better? With 1.3 billion dropoff locations, a scatter plot is essentially useless,  -->
  </div>
</article>
<hr>
<div id="disqus_thread"></div>
<script>
  var disqus_config = function() {
    this.page.url = '../posts/incandescent-taxi-view.html';
    this.page.identifier = 'incandescent-taxi-view';
  };
  (function() {
    var d = document;
    var s = d.createElement('script');
    s.src = '//dataalley.disqus.com/embed.js';
    s.setAttribute('data-timestamp', +new Date());
    (d.head || d.body).appendChild(s);
  })();
</script>
<noscript class="text-muted">
  Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a>
</noscript>
    </div>
  </div>

  <footer class="footer">
    <div class="container">
      <div class="row">
       <ul class="col-sm-6 list-inline">
          <li class="list-inline-item"><a href="../archives.html">Archives</a></li>
          <li class="list-inline-item"><a href="../categories.html">Categories</a></li>
          <li class="list-inline-item"><a href="../tags.html">Tags</a></li>
        </ul>
        <p class="col-sm-6 text-sm-right text-muted">
          Generated by <a href="https://github.com/getpelican/pelican" target="_blank">Pelican</a> / <a href="https://github.com/nairobilug/pelican-alchemy" target="_blank">&#x2728;</a>
        </p>
      </div>
    </div>
  </footer>
</body>

  <style type="text/css">
/* Overrides of notebook CSS for static HTML export */
div.entry-content {
overflow: visible;
padding: 8px;
}
.input_area {
padding: 0.2em;
}
a.heading-anchor {
white-space: normal;
}
.rendered_html
code {
font-size: .8em;
}
pre.ipynb {
color: black;
background: #f7f7f7;
border: none;
box-shadow: none;
margin-bottom: 0;
padding: 0;
margin: 0px;
font-size: 13px;
}
/* remove the prompt div from text cells */
div.text_cell .prompt {
display: none;
}
/* remove horizontal padding from text cells, */
/* so it aligns with outer body text */
div.text_cell_render {
padding: 0.5em 0em;
}
img.anim_icon {
padding: 0;
border: 0;
vertical-align: middle;
-webkit-box-shadow: none;
-box-shadow: none
}
div.collapseheader {
width=100%;
background-color: #d3d3d3;
padding: 2px;
cursor: pointer;
font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
}
</style>
<script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML"></script>
<script type="text/javascript">
init_mathjax = function() {
if (window.MathJax) {
// MathJax loaded
MathJax.Hub.Config( {
tex2jax: {
inlineMath: [ ['$','$'], ["\\(","\\)"] ],
displayMath: [ ['$$','$$'], ["\\[","\\]"] ]
}
,
displayAlign: 'left', // Change this to 'center' to center equations.
"HTML-CSS": {
styles: {'.MathJax_Display': {"margin": 0
}

}

}

}
);
MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
}

}
init_mathjax();
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
$("div.collapseheader").click(function () {
$header = $(this).children("span").first();
$codearea = $(this).children(".input_area");
console.log($(this).children());
$codearea.slideToggle(500, function () {
$header.text(function () {
return $codearea.is(":visible") ? "Collapse Code" : "Expand Code";
}
);
}
);
}
);
}
);
</script>
  
</html>