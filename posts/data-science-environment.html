<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags always come first -->
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>A Reproducible Data Science Environment in Ubuntu 16.04 with Python, R, Spark, and Docker | Ravi Shekhar's Technical Blog
</title>
  <link rel="canonical" href="../posts/data-science-environment.html">

  <link rel="alternate" type="application/atom+xml" href="https://r-shekhar.github.io/feeds/all.atom.xml" title="Full Atom Feed">


  <link rel="stylesheet" href="../theme/css/bootstrap.min.css">
  <link rel="stylesheet" href="../theme/css/font-awesome.min.css">
  <link rel="stylesheet" href="../theme/css/pygments/default.min.css">
  <link rel="stylesheet" href="../theme/css/style.css">


<meta name="description" content="A step by step tutorial for setting up a reproducible data science environment with Python, R, Apache Spark, and Docker on Ubuntu 16.04 LTS. All steps valid for both local bare-metal installs and on cloud services like Amazon EC2.">
<script>
  (function(i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function() {
      (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o);
    a.async = 1;
    a.src = g;
    m = s.getElementsByTagName(o)[0];
    m.parentNode.insertBefore(a, m)
  })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
  ga('create', 'UA-8241620-2', 'auto');
  ga('send', 'pageview');
</script>
</head>

<body>
  <header class="header">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <a href="../">
            <img class="img-fluid" src=../images/Self_small.jpg width=150 alt="Ravi Shekhar's Technical Blog">
          </a>
        </div>
        <div class="col-sm-8">
          <h1 class="title"><a href="../">Ravi Shekhar's Technical Blog</a></h1>
          <p class="text-muted">A Technical Blog of the Data Science Process</p>
          <ul class="list-inline">
            <li class="list-inline-item"><a href="https://github.com/r-shekhar" target="_blank">Github</a></li>
            <li class="list-inline-item"><a href="https://www.linkedin.com/in/rshekhar2/" target="_blank">LinkedIn</a></li>
            <li class="list-inline-item text-muted">|</li>
            <li class="list-inline-item"><a href="../pages/about.html">About</a></li>
          </ul>
        </div>
      </div>
    </div>
  </header>

  <div class="main">
    <div class="container">
      <h1>A Reproducible Data Science Environment in Ubuntu 16.04 with Python, R, Spark, and Docker
</h1>
      <hr>
<article class="article">
  <header>
    <ul class="list-inline">
      <li class="list-inline-item text-muted" title="2017-05-01T00:00:00-04:00">
        <i class="fa fa-clock-o"></i>
        2017 May 01
      </li>
      <li class="list-inline-item">
        <i class="fa fa-folder-open-o"></i>
        <a href="../category/software.html">Software</a>
      </li>
      <li class="list-inline-item">
        <i class="fa fa-files-o"></i>
        <a href="../tag/python.html">#python</a>,         <a href="../tag/r.html">#R</a>,         <a href="../tag/spark.html">#Spark</a>      </li>
    </ul>
  </header>
  <div class="content">
    <p>Data Science is heavily reliant on fairly complex software environments in which a minor version bump within one software package can change the ultimate data science result. For reproducibility, it's very useful to have an environment with well defined versions of common packages that is easy to replicate. As data science typically involves a pipeline, it is not uncommon to have different pipeline stages written in different languages, such as R and Python, with an interoperability layer (such as <code>rpy2</code>) linking them. This dual-language environment is especially difficult to version control and reproduce. Here is my solution to a Python/Spark/R environment, mostly based on Anaconda. </p>
<p>I have tested the entire tutorial on a local workstation, and on Amazon EC2. I post this mostly for my own documentation, but it may be useful to others as well.</p>
<h2>Step 0</h2>
<p>Install Ubuntu 16.04 LTS, and execute the following.</p>
<p>Setup SSH. You can skip the following step if running on EC2.</p>
<div class="highlight"><pre><span></span>sudo apt-get install openssh-server

<span class="c1"># setup easy login to localhost</span>
ssh-keygen -t rsa -b <span class="m">4096</span>
cat ~/.ssh/id_rsa.pub &gt;&gt; ~/.ssh/authorized_keys
chmod <span class="m">600</span> ~/.ssh/authorized_keys
</pre></div>


<p>Update the system and reboot</p>
<div class="highlight"><pre><span></span>sudo apt-get update <span class="o">&amp;&amp;</span> sudo apt-get upgrade
sudo reboot
</pre></div>


<h2>Step 1 : Java</h2>
<p><strong>Note:</strong> If you have no interest in Spark, you can skip this step.</p>
<p>Install Java. This step is first because sometimes packages can require openJDK,
which does not work as well with Spark as Oracle Java. </p>
<div class="highlight"><pre><span></span>sudo add-apt-repository -y ppa:webupd8team/java
sudo apt-get update
sudo apt-get -y install oracle-java8-installer
sudo apt-get install -y oracle-java8-set-default
</pre></div>


<h2>Step 2 : Anaconda</h2>
<p>Install Anaconda3 to get Python. We will later install R through Anaconda as well.</p>
<div class="highlight"><pre><span></span>wget <span class="s1">&#39;http://repo.continuum.io/archive/Anaconda3-4.2.0-Linux-x86_64.sh&#39;</span>
bash Anaconda3*sh -b
<span class="nb">echo</span> <span class="s1">&#39;export PATH=${HOME}/anaconda3/bin:$PATH&#39;</span> &gt;&gt; ~/.bashrc
</pre></div>


<p>I strongly recommend using the version of Anaconda indicated above, and not a later version. Apache Spark / PySpark packages require Python 3.5, whereas more recent Anaconda installations use Python 3.6. </p>
<p>These are a set of packages that I use </p>
<div class="highlight"><pre><span></span>conda install -c conda-forge fastparquet geopandas fiona snappy python-snappy\
    bokeh dask distributed numba scikit-learn pyarrow matplotlib palettable\
    seaborn bottleneck

pip install git+https://github.com/pymc-devs/pymc3
pip install brewer2mpl
</pre></div>


<h2>Step 3 : Spark</h2>
<p>Install Spark 2.0.2 using Anaconda</p>
<div class="highlight"><pre><span></span>conda install -c quasiben spark=2.0.2 -y
</pre></div>


<p>The following set of environment variables will set <code>jupyter</code> as the the pyspark driver. Add it to your <code>.bashrc</code></p>
<div class="highlight"><pre><span></span>#HADOOP VARIABLES START
JAVA_HOME=`which java`
JAVA_HOME=`readlink -f <span class="cp">${</span><span class="n">JAVA_HOME</span><span class="cp">}</span>`
export JAVA_HOME=`echo <span class="cp">${</span><span class="n">JAVA_HOME</span><span class="cp">}</span> | sed -e &#39;s/\/bin\/java//&#39;`

export JRE_HOME=<span class="cp">${</span><span class="n">JAVA_HOME</span><span class="cp">}</span>
export PYSPARK_SUBMIT_ARGS=&quot;--master &#39;local[4]&#39; --executor-memory 3G --driver-memory 3G&quot;
export PYSPARK_DRIVER_PYTHON=&quot;jupyter&quot;
export PYSPARK_DRIVER_PYTHON_OPTS=&quot;notebook --no-browser &quot;
export SPARK_DRIVER_PYTHON=<span class="cp">${</span><span class="n">HOME</span><span class="cp">}</span>/anaconda3/bin/python
</pre></div>


<p>Adjust Spark to use the number of cores on your machine using <code>local[N]</code>, as well as the executor and driver memory. With a 16GB memory machine with 4 cores, 3 GB for driver and executor memory is a good rule of thumb. </p>
<h2>Step 4 : R</h2>
<p>Continuum, the company behind the Anaconda Python distribution, has produced an R channel for anaconda, where many (most?) common packages are available.</p>
<div class="highlight"><pre><span></span>conda install -c r r 
conda install -c r rpy2 r-tidyverse r-shiny r-essentials r-sparklyr <span class="se">\</span>
    r-feather r-markdown r-knitr r-spatial r-rstan r-rbokeh r-maps <span class="se">\</span>
    r-hexbin r-ggvis
</pre></div>


<h4>[Optional: RStudio]</h4>
<p>Following <a href="https://www.r-bloggers.com/how-to-install-r-on-linux-ubuntu-16-04-xenial-xerus/">R-bloggers</a></p>
<div class="highlight"><pre><span></span>sudo echo &quot;deb http://cran.rstudio.com/bin/linux/ubuntu xenial/&quot; | sudo tee -a /etc/apt/sources.list
sudo gpg --keyserver keyserver.ubuntu.com --recv-key E084DAB9
sudo gpg -a --export E084DAB9 | sudo apt-key add -
sudo apt-get update

sudo apt-get install gdebi-core
wget https://download1.rstudio.org/rstudio-1.0.143-amd64.deb
sudo gdebi -n rstudio-1.0.143-amd64.deb
rm rstudio-1.0.143-amd64.deb
</pre></div>


<p>If you want RStudio-server, it can be installed with the guide <a href="https://www.r-bloggers.com/installing-rstudio-server-on-ubuntu-server/">here</a>.</p>
<h2>Step 5 : Archive the Anaconda Environment</h2>
<p>The following command will produce a list of packages installed in the Anaconda environment. You can find more documentation <a href="https://conda.io/docs/using/envs.html">from Continuum</a></p>
<div class="highlight"><pre><span></span>conda list --explicit &gt; spec-file.txt
</pre></div>


<p>Save this spec file that was just created. It can be used to create a new environment on another machine with the same operating system. </p>
<div class="highlight"><pre><span></span>conda create --name MyEnvironment --file spec-file.txt
</pre></div>


<p>I have made my spec-file <a href="https://gist.github.com/r-shekhar/12ec0bc8954333c3745c255dd4889e14">available on Github</a>. </p>
<h2>Step 6 : [Optional] Docker</h2>
<p>Install Docker</p>
<div class="highlight"><pre><span></span>sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
sudo <span class="nb">echo</span> <span class="s1">&#39;deb https://apt.dockerproject.org/repo ubuntu-xenial main&#39;</span> <span class="p">|</span> sudo tee -a /etc/apt/sources.list.d/docker.list
sudo apt-get update <span class="o">&amp;&amp;</span> sudo apt-get install -y docker-engine
</pre></div>


<h2>Step 7 : Optional [EC2] — Create a Custom AMI</h2>
<p>A custom AMI (Amazon Machine Image) allows creation of a bootable image from a running EBS-backed instance. If all previous steps in this tutorial have been run on EC2, <a href="http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/creating-an-ami-ebs.html">Amazon's documentation</a> shows how to create an image from a running instance, which can be used to spin up additional identical instances. Their documentation is excellent, so I do not rehash it here. </p>
<h1>Conclusion</h1>
<p>Setting up an environment that works is a very time consuming and inconvenient part of data science. This post represents the results of a lot of trial and error at obtaining an environment that is stable and reproducible. Using the directions here, it should be possible to create an environment that is identical to the one I will use for data analyses I publish on this blog. </p>
  </div>
</article>
    </div>
  </div>

  <footer class="footer">
    <div class="container">
      <div class="row">
       <ul class="col-sm-6 list-inline">
          <li class="list-inline-item"><a href="../archives.html">Archives</a></li>
          <li class="list-inline-item"><a href="../categories.html">Categories</a></li>
          <li class="list-inline-item"><a href="../tags.html">Tags</a></li>
        </ul>
        <p class="col-sm-6 text-sm-right text-muted">
          Generated by <a href="https://github.com/getpelican/pelican" target="_blank">Pelican</a> / <a href="https://github.com/nairobilug/pelican-alchemy" target="_blank">&#x2728;</a>
        </p>
      </div>
    </div>
  </footer>
</body>

  <style type="text/css">
/* Overrides of notebook CSS for static HTML export */
div.entry-content {
overflow: visible;
padding: 8px;
}
.input_area {
padding: 0.2em;
}
a.heading-anchor {
white-space: normal;
}
.rendered_html
code {
font-size: .8em;
}
pre.ipynb {
color: black;
background: #f7f7f7;
border: none;
box-shadow: none;
margin-bottom: 0;
padding: 0;
margin: 0px;
font-size: 13px;
}
/* remove the prompt div from text cells */
div.text_cell .prompt {
display: none;
}
/* remove horizontal padding from text cells, */
/* so it aligns with outer body text */
div.text_cell_render {
padding: 0.5em 0em;
}
img.anim_icon {
padding: 0;
border: 0;
vertical-align: middle;
-webkit-box-shadow: none;
-box-shadow: none
}
div.collapseheader {
width=100%;
background-color: #d3d3d3;
padding: 2px;
cursor: pointer;
font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
}
</style>
<script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML"></script>
<script type="text/javascript">
init_mathjax = function() {
if (window.MathJax) {
// MathJax loaded
MathJax.Hub.Config( {
tex2jax: {
inlineMath: [ ['$','$'], ["\\(","\\)"] ],
displayMath: [ ['$$','$$'], ["\\[","\\]"] ]
}
,
displayAlign: 'left', // Change this to 'center' to center equations.
"HTML-CSS": {
styles: {'.MathJax_Display': {"margin": 0
}

}

}

}
);
MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
}

}
init_mathjax();
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
$("div.collapseheader").click(function () {
$header = $(this).children("span").first();
$codearea = $(this).children(".input_area");
console.log($(this).children());
$codearea.slideToggle(500, function () {
$header.text(function () {
return $codearea.is(":visible") ? "Collapse Code" : "Expand Code";
}
);
}
);
}
);
}
);
</script>
  
</html>